import { combineReducers } from "redux";
import { currentTableReducer, singleRegisterReducer, descriptiveDataReducer } from "./currentTable.reducer";
import { notificationReducer } from "./notification.reducer";

const appReducers = combineReducers({
  currentTableReducer,
  notificationReducer,
  singleRegisterReducer,
  descriptiveDataReducer
});

export default appReducers;
