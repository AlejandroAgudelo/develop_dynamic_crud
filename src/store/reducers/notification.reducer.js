import { notificationsConstants } from "../../constants";

export function notificationReducer(state={},action){
    switch(action.type){
        case notificationsConstants.ERROR_GLOBAL:
            return { ...state, error: true, msg:action.payload }
        case notificationsConstants.SUCCESS_GLOBAL:
            return { ...state, success: true, msg:action.payload }
        case notificationsConstants.CLEAR_NOTIFICATION:
            return {}
        default:
            return state
    }
}