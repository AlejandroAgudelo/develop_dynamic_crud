import { jsonDataConstants } from "../../constants";

export function currentTableReducer(state = {}, action) {
  switch (action.type) {
    case jsonDataConstants.GET_JSON_DATA_REQUEST:
      return {
        loadingData: true,
      };
    case jsonDataConstants.GET_JSON_DATA_SUCCESS:
      return { result: action.result };
    case jsonDataConstants.GET_JSON_DATA_FAILED:
      return {
        errorLoadingData: action.error,
      };
    default:
      return state;
  }
}

export function singleRegisterReducer(state = {}, action) {
  switch (action.type) {
    case jsonDataConstants.GET_REGISTER_DATA_REQUEST:
      return {
        loadingSingleRegister: true,
      };
    case jsonDataConstants.GET_REGISTER_DATA_SUCCESS:
      return {
        result: action.result,
      };
    case  jsonDataConstants.GET_REGISTER_DATA_FAILED:
      return {
        errorSingleRegister: action.error,
      };
    case jsonDataConstants.CLEAR_SINGLE_REGISTER_DATA:
      return {
        result: "",
      };
    default:
      return state;
  }
}

export function descriptiveDataReducer(state = {} , action) {
  switch (action.type) {
    case jsonDataConstants.GET_DESCRIPTIVE_DATA_REQUEST:
      return {
        loadingSingleRegister: true,
      };
    case jsonDataConstants.GET_DESCRIPTIVE_DATA_SUCCESS:
      return { result: {...state.result, [action.result.columnName]:action.result} }

    case  jsonDataConstants.GET_DESCRIPTIVE_DATA_FAILED:
      return {
        errorSingleRegister: action.error,
      };
    case jsonDataConstants.CLEAR_DESCRIPTIVE_DATA:
      return {
        result: "",
      };
    default:
      return state;
  }
}
