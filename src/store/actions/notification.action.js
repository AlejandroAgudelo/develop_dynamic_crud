import { notificationsConstants } from "../../constants";
// For notification

export const errorGlobal = (msg) => ({
  type: notificationsConstants.ERROR_GLOBAL,
  payload: msg,
});

export const successGlobal = (msg) => ({
  type: notificationsConstants.SUCCESS_GLOBAL,
  payload: msg,
});

export const clearNotification = () => ({
  type: notificationsConstants.CLEAR_NOTIFICATION,
});
