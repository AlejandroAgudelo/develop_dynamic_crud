import { jsonDataConstants } from "../../constants";

import * as notificationActions from './notification.action';

import { services } from '../services';
export const getJsonData = (RequestConfiguration, serviceConfiguration) => {
  return dispatch => {
    
    dispatch(request());
    services.getPaginatedApprovalsAttorneys(RequestConfiguration,serviceConfiguration)
      .then(
        result => dispatch(success(result)),
        error => dispatch(failure(error.message)),
      );
  };
  function request() { return { type: jsonDataConstants.GET_JSON_DATA_REQUEST } }
  function success(result) { return { type: jsonDataConstants.GET_JSON_DATA_SUCCESS, result } }
  function failure(error) { return { type: jsonDataConstants.GET_JSON_DATA_FAILED, error } }

};

const addNewData = (newDataObject, serviceConfiguration) => {
  return dispatch => {
  dispatch(request());
  services.addNewData(newDataObject, serviceConfiguration)
  .then(
        result => {dispatch(success(result));dispatch(notificationActions.successGlobal("Right, data saved"));},
        error => {dispatch(failure(error.message));dispatch(notificationActions.errorGlobal(`Error, data was not saved. ${error.message}`))},
      );
  };
  function request()      { return { type: jsonDataConstants.ADD_NEW_DATA_REQUEST } }
  function success(result){ return { type: jsonDataConstants.ADD_NEW_DATA_SUCCESS, result } }
  function failure(error) { return { type: jsonDataConstants.ADD_NEW_DATA_FAILED, error } }

};

function updateData( dataObject, PKName, PKValue, serviceConfiguration ) {
  return dispatch => {
    
    dispatch(request());
    services.updateData(dataObject, PKName, PKValue, serviceConfiguration)
      .then(
        result => {dispatch(success(result));dispatch(notificationActions.successGlobal("Right, data updated"));},
        error => {dispatch(failure(error.message));dispatch(notificationActions.errorGlobal(`Error: data was not updated. ${error.message}`))},
      );
  };
  function success(result){ return { type: jsonDataConstants.UPDATE_DATA_SUCCESS, result } }
  function failure(error) { return { type: jsonDataConstants.UPDATE_DATA_FAILED, error } }
  function request()      { return { type: jsonDataConstants.UPDATE_DATA_REQUEST } }
  
}

function getSingleRegisterData( filterFeature, value, serviceConfiguration ) {

  return dispatch => {    
    dispatch(request());
    services.getSingleRegisterData( filterFeature, value, serviceConfiguration )
      .then(
        result => dispatch(success(result)),
        error => dispatch(failure(error.message)),
      );
  };
  function request()      { return { type: jsonDataConstants.GET_REGISTER_DATA_REQUEST } }
  function success(result){ return { type: jsonDataConstants.GET_REGISTER_DATA_SUCCESS, result } }
  function failure(error) { return { type: jsonDataConstants.GET_REGISTER_DATA_FAILED, error } }

}

function clearSingleRegisterData() {
  return {
    type: jsonDataConstants.CLEAR_SINGLE_REGISTER_DATA
  }
}

function getDescriptiveData( descriptionModel, descriptionField, columnName ) {
  return dispatch => {    
    dispatch(request());
    services.getDescriptiveData( descriptionModel, descriptionField )
      .then(
        result => dispatch(success(result)),
        error => dispatch(failure(error.message)),
      );
  };
  function request()      { return { type: jsonDataConstants.GET_DESCRIPTIVE_DATA_REQUEST } }
  function success(result){ return { type: jsonDataConstants.GET_DESCRIPTIVE_DATA_SUCCESS, result:{...result, columnName:columnName} } }
  function failure(error) { return { type: jsonDataConstants.GET_DESCRIPTIVE_DATA_FAILED, error } }

}


export const currentTableActions = {
  getJsonData,
  addNewData,
  updateData,
  getSingleRegisterData,
  clearSingleRegisterData,
  getDescriptiveData
};