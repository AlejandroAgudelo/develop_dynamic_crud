function handleResponse(response) {
  return response.text().then(text => {
    if (!response.ok) {
      return Promise.reject(JSON.parse(text));
    }
    const data = text && JSON.parse(text);
    return data;
  });
}

// Approval Attorney Page
function getPaginatedApprovalsAttorneys(RequestConfiguration,serviceConfiguration){

    const requestOptions = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(RequestConfiguration)
    };
    // return fetch(`${config.documentBuilderUrl}/${serviceConfiguration.tableName}/getDataByPage`, requestOptions).then(handleResponse);
    return fetch(`http://localhost:3003/${serviceConfiguration.tableName}/getDataByPage`, requestOptions).then(handleResponse);
  }

  function addNewData(newDataObject,serviceConfiguration){
    const requestOptions = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(newDataObject)
    };
    // return fetch(`${config.documentBuilderUrl}/approval/addApprovalAttorney`, requestOptions).then(handleResponse);
    return fetch(`http://localhost:3003/${serviceConfiguration.tableName}/addNewData`, requestOptions).then(handleResponse);
  }

  function updateData(dataObject, PKName, PKValue, serviceConfiguration){
    console.log(JSON.stringify({PKName:PKName,PKValue:PKValue,dataObject:dataObject}))
    const requestOptions = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({PKName:PKName,PKValue:PKValue,dataObject:dataObject})
      // body: JSON.stringify(dataObject)
    };
    // return fetch(`${config.documentBuilderUrl}/approval/updateApprovalAttorney/${id}`, requestOptions).then(handleResponse);
    return fetch(`http://localhost:3003/${serviceConfiguration.tableName}/updateData/`, requestOptions).then(handleResponse);
  }

  function getSingleRegisterData(filterFeature, value, serviceConfiguration){
    const requestOptions = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({filterFeature:filterFeature, value:value})
    };
    // return fetch(`${config.documentBuilderUrl}/approval/getApprovalAttorney`, requestOptions).then(handleResponse);
    return fetch(`http://localhost:3003/${serviceConfiguration.tableName}/singleRegisterData`, requestOptions).then(handleResponse);
  }

  function getDescriptiveData( descriptionModel, descriptionField ){
    const requestOptions = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({descriptionModel:descriptionModel, descriptionField:descriptionField})
    };
    // return fetch(`${config.documentBuilderUrl}/approval/getApprovalAttorney`, requestOptions).then(handleResponse);
    return fetch(`http://localhost:3003/getDescriptiveData`, requestOptions).then(handleResponse);
  }


  export const services = {
      getPaginatedApprovalsAttorneys,
      addNewData,
      updateData,
      getSingleRegisterData,
      getDescriptiveData
  }

  