/**
 * @author John Agudelo
 * @file Manages the differents options on the CRUD dynamic table
 */
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link as RouterLink } from "react-router-dom";
import {
  IconButton,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
  Container,
  TableFooter,
  TablePagination,
  Grid,
  Button,
} from "@material-ui/core";
import Moment from "moment";
import "./styles.css";

import EditIcon from "@material-ui/icons/Edit";
import AddIcon from "@material-ui/icons/Add";
import SearchIcon from "@material-ui/icons/Search";
import SimpleBackdrop from "../tools/backdrop";

//Actions
import { currentTableActions } from "../../store/actions";
import { transactionalActions } from "../../constants/actions.constants";

/** Hashmap to keep an alignment depending on the kind of value incoming */
const aligItems = {
  string: "left",
  number: "right",
  date: "center",
  boolean: "center",
};

const DynamicTable = (props) => {
  const tableTitle = props.match.params.tableTitle;
  const tableName = props.match.params.tableName;

  const [paginationState, setPaginationState] = useState({
    sortBy: "name",
    order: "asc",
    limit: 5,
    skip: 0,
    search: "",
  });
  const [dataCount, setDataCount] = useState(0);
  const [currentPage, setCurrentPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(5);

  const dispatch = useDispatch();

  const currentTableReducer = useSelector((state) => state.currentTableReducer);

  useEffect(() => {
    let serviceConfiguration = { tableName: tableName };
    dispatch(
      currentTableActions.getJsonData(paginationState, serviceConfiguration)
    );
    if (currentTableReducer && currentTableReducer.result) {
      setDataCount(currentTableReducer.result.amount);
      let columnList = [];
      currentTableReducer.result.model.map((item) =>
        columnList.push(item.columnName)
      );
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [paginationState, tableName]);

  const handleEdit = (id) => {
    let action = transactionalActions.EDIT;
    props.history.push(`/${tableName}/${tableTitle}/${action}/${id}`);
  };

  const handleChangePage = (event, newPage) => {
    if (newPage > currentPage) {
      setPaginationState({
        ...paginationState,
        skip: parseInt(paginationState.skip) + parseInt(paginationState.limit),
      });
      setCurrentPage(newPage);
    } else {
      setPaginationState({
        ...paginationState,
        skip: parseInt(paginationState.skip) - parseInt(paginationState.limit),
      });
      setCurrentPage(newPage);
    }
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setCurrentPage(0);
    setPaginationState({
      ...paginationState,
      limit: parseInt(event.target.value),
    });
  };

  // const newOrder = (columName) => {
  //   setSortby(columName);
  //   if (orderDir === "desc") {
  //     setOrderDir("asc");
  //   } else {
  //     setOrderDir("desc");
  //   }
  //   setPaginationState({
  //     ...paginationState,
  //     sortBy: columName,
  //     order: orderDir,
  //   });
  // };

  return (
    <>
      {currentTableReducer.loadingData ? (
        <SimpleBackdrop />
      ) : (
        <Container>
          <Grid
            container
            justify="flex-end"
            alignItems="center"
            className="gridActionsContainer"
          >
            <Grid item xs={8}>
              <h3 className="tableTitle">{tableTitle}</h3>
            </Grid>
            <Grid item xs={3}>
              <Grid container spacing={1} alignItems="center">
                <Grid item>
                  <SearchIcon />
                </Grid>
                {/* <Grid item >
                  <TextField
                    id="input-with-icon-grid"
                    label="Buscar texto"
                    style={{ marginTop: "-1rem" }}
                    value={searchValue}
                    onChange={handleChangeSearch}
                  />
                </Grid> */}
              </Grid>
            </Grid>
            <Grid item xs={1}>
              <Button
                className="addBtn"
                component={RouterLink}
                to={`/${tableName}/${tableTitle}/${transactionalActions.ADD}/0`}
                variant="contained"
              >
                <AddIcon />
                Add
              </Button>
            </Grid>
          </Grid>
          <TableContainer component={Paper}>
            <Table className="table" aria-label="a dense table">
              <TableHead className="tableHead">
                <TableRow>
                  {currentTableReducer && currentTableReducer.result
                    ? currentTableReducer.result.model.map((item) =>
                        item.props.showInTable ? (
                          item.props.isDescriptedByForeign ? (
                            <TableCell
                              key={item.columnName}
                              align={aligItems[item.props.descriptionFieldType]}
                            >
                              <strong>{item.columnDescriptiviveName}</strong>
                            </TableCell>
                          ) : (
                            <TableCell
                              key={item.columnName}
                              align={aligItems[item.columnType]}
                            >
                              <strong>{item.columnDescriptiviveName}</strong>
                            </TableCell>
                          )
                        ) : null
                      )
                    : null}
                  <TableCell>
                    <strong>Edit</strong>{" "}
                  </TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {currentTableReducer && currentTableReducer.result
                  ? currentTableReducer.result.data.map((row, i) => (
                      <TableRow key={i}>
                        {Object.entries(row).map((item, index) =>
                          /** item[0] has the field name */
                          {
                            let currentColumn = currentTableReducer.result.model.find( (element) => element.columnName === item[0] );
                            let value;
                            if ( currentColumn !== undefined && currentColumn.columnType === "date" ) {
                              value = Moment(item[1]).format("YYYY-MM-DD");
                            } else {
                              if ( currentColumn !== undefined && currentColumn.props.isDescriptedByForeign ) {
                                let currentElement = currentTableReducer.result.data.find((element)=>(element[item[0]] === item[1]))
                                value = currentElement[currentColumn.props.descriptionModel][currentColumn.props.descriptionField]
                                value = value.toString().toUpperCase()
                              } else {
                                if ( currentColumn !== undefined  && currentColumn.columnType === "boolean" ){
                                  value = item[1] === true ? 'SI':'NO';
                                }else{
                                  value = item[1].toString().toUpperCase();
                                }
                              }
                            }
                            return currentColumn !== undefined ? (
                              currentColumn.props.showInTable ? (
                                <TableCell
                                  key={item[1] + item[0]+index}
                                  align={currentColumn.props.isDescriptedByForeign ? aligItems[currentColumn.props.descriptionFieldType] : aligItems[currentColumn.columnType] }
                                >
                                  {value}
                                </TableCell>
                              ) : null
                            ) : null;
                          }
                        )}
                        <TableCell>
                          <IconButton onClick={() => handleEdit(row.id)}>
                            <EditIcon />
                          </IconButton>
                        </TableCell>
                      </TableRow>
                    ))
                  : null}
              </TableBody>
              <TableFooter>
                <TableRow>
                  {currentTableReducer.result ? (
                    <TablePagination
                      rowsPerPageOptions={[5, 10, 25, 50, 100]}
                      // colSpan={2}
                      count={dataCount}
                      rowsPerPage={rowsPerPage}
                      page={currentPage}
                      SelectProps={{
                        inputProps: { "aria-label": "rows per page" },
                        native: true,
                      }}
                      onChangePage={handleChangePage}
                      onChangeRowsPerPage={handleChangeRowsPerPage}
                      labelRowsPerPage="Rows per page"
                    />
                  ) : null}
                </TableRow>
              </TableFooter>
            </Table>
          </TableContainer>
        </Container>
      )}
    </>
  );
};

export { DynamicTable };
