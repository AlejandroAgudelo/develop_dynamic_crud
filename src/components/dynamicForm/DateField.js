import React from "react";
import { KeyboardDatePicker } from "@material-ui/pickers";
import { FormHelperText, Grid, FormControl } from "@material-ui/core";

export const DateField = ({
  titleField,
  nameField,
  formsValues,
  handleDates,
  formHelpers,
}) => {
  return (
    <Grid item xs={6}>
      <div className="inputForm">
        <FormControl style={{ width: "100%" }}>
          <KeyboardDatePicker
            error={formHelpers[nameField] ? true : false}
            inputVariant="outlined"
            margin="normal"
            id="date-picker-dialog"
            label={titleField}
            format="yyyy/MM/dd"
            value={formsValues[nameField]}
            onChange={(event) => {
              handleDates({ event, nameField });
            }}
            KeyboardButtonProps={{
              "aria-label": "change date",
            }}
            style={{ marginTop: "0rem" }}
          />
          <FormHelperText
            id={"helper" + nameField}
            style={{ color: "#F01C0F" }}
          >
            {formHelpers[nameField] ? formHelpers[nameField] : null}
          </FormHelperText>
        </FormControl>
      </div>
    </Grid>
  );
};
