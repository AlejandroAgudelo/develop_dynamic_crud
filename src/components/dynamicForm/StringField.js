import React from "react";
import {
  FormHelperText,
  Grid,
  FormControl,
  TextField,
} from "@material-ui/core";


export const StringField = ({
  titleField,
  nameField,
  handleFormsValue,
  formsValues,
  formHelpers,
}) => {
  return (
    <Grid item xs={6}>
      <div className="inputForm">
        <FormControl style={{ width: "100%" }}>
          <TextField
            error = {formHelpers[nameField] ? true : false}
            id={nameField}
            aria-describedby="my-helper-text"
            onChange={handleFormsValue}
            name={nameField}
            value={formsValues[nameField] || ""}
            variant="outlined"
            label={titleField}
          />
          <FormHelperText id={"helper" + nameField} style={{color:"#F01C0F"}}>
            {formHelpers[nameField] ? formHelpers[nameField] : null}
          </FormHelperText>
        </FormControl>
      </div>
    </Grid>
  );
};
