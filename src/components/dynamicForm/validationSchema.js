import Moment from "moment";

export const ValidationSchema = (field, value, formsValues) => {
  let errorMsg = null;
  let upper = false;
  switch (field.columnType) {
    case "date":
      value = Moment(value).format("YYYY-MM-DDTHH:mm:ss");
      if (field.validations) {
        for (let singleValidation of Object.entries(field.validations)) {
          if (singleValidation[0] === "isRequired") {
            if (value === "") {
              errorMsg = "Can't be empty";
              break;
            }
          }
          if (singleValidation[0] === "smallerThan") {
            let secondDate = formsValues[singleValidation[1]];
            secondDate = Moment(secondDate).format("YYYY-MM-DDTHH:mm:ss");
            if (value > secondDate) {
              errorMsg = "Invalid dates";
              break;
            }
          }
          if (singleValidation[0] === "greaterThan") {
            let secondDate = formsValues[singleValidation[1]];
            secondDate = Moment(secondDate).format("YYYY-MM-DDTHH:mm:ss");
            if (value < secondDate) {
              errorMsg = "Invalid dates";
              break;
            }
          }
        }
      }
      break;
    case "email":
      if (
        !/^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(
          value
        )
      ) {
        errorMsg = "It is not an valid email";
      }
      if (field.validations) {
        for (let singleValidation of Object.entries(field.validations)) {
          if (singleValidation[0] === "isRequired") {
            if (value === "") {
              errorMsg = "Can't be empty";
              break;
            }
          }
          if (singleValidation[0] === "maxLength") {
            if (value.length >= singleValidation[1]) {
              errorMsg = "Too many characters";
              break;
            }
          }
          if (singleValidation[0] === "minLength") {
            if (value.length < singleValidation[1]) {
              errorMsg = "Too few characters";
              break;
            }
          }
        }
      }
      break;
    case "number":
      if (/[a-zA-Z.!#$%&’*+/=?^_`{|}~-]/.test(value)) {
        errorMsg = "Only numbers allowed";
      }
      if (field.validations) {
        for (let singleValidation of Object.entries(field.validations)) {
          if (singleValidation[0] === "isRequired") {
            if (value === "" || value === null) {
              errorMsg = "Can't be empty";
              break;
            }
          }
          if (singleValidation[0] === "under") {
            if (value > singleValidation[1]) {
              errorMsg = `Value out of range (value < ${singleValidation[1]})`;
              break;
            }
          }
          if (singleValidation[0] === "atLeast") {
            if (value < singleValidation[1]) {
              errorMsg = `Value out of range (value > ${singleValidation[1]})`;
              break;
            }
          }
        }
      }
      break;
    case "string":
      if (field.validations) {
        for (let singleValidation of Object.entries(field.validations)) {
          if (singleValidation[0] === "isRequired") {
            if (value === "") {
              errorMsg = "Can't be empty";
              break;
            }
          }
          if (singleValidation[0] === "upperCase" && singleValidation[1]) {
            upper = true;
          }
          if (singleValidation[0] === "maxLength") {
            if (value.length >= singleValidation[1]) {
              errorMsg = "Too many characters";
              break;
            }
          }
          if (singleValidation[0] === "minLength") {
            if (value.length < singleValidation[1]) {
              errorMsg = "Too few characters";
              break;
            }
          }
          if (singleValidation[0] === "numbersOnly") {
            if (singleValidation[1] && !/^[0-9]/.test(value)) {
              errorMsg = "Only numbers allowed";
              break;
            }
          }
        }
      }
      value = upper ? value.toUpperCase() : value;
      break;
    default:
      break;
  }
  return [errorMsg, value];
};
