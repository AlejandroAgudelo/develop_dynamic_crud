
import React from "react";
import {
  FormControl as FormControlMaterial,
  FormHelperText,
  Grid,
  Switch,
} from "@material-ui/core";

export const BooleanField = ({
  titleField,
  nameField,
  stateSwitch,
  handleChangeSwitch,
  formHelpers,
  formsValues
}) => {
  return (
    <Grid item xs={6}>
      <div className="inputForm">
        <FormHelperText>{titleField}</FormHelperText>
        <FormControlMaterial className="txtFieldForm">
          <Grid component="label" container alignItems="center" spacing={1}>
            <Grid item>False</Grid>
            <Grid item>
              <Switch
                checked={formsValues[nameField] || false}
                onChange={handleChangeSwitch}
                color="primary"
                name={nameField}
                inputProps={{ "aria-label": "primary checkbox" }}
              />
            </Grid>
            <Grid item>True</Grid>
          </Grid>
          <FormHelperText
            id={"helper" + nameField}
            style={{ color: "#F01C0F" }}
          >
            {formHelpers[nameField] ? formHelpers[nameField] : null}
          </FormHelperText>
        </FormControlMaterial>
      </div>
    </Grid>
  );
};
