/* eslint-disable array-callback-return */
import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { useFormik } from "formik";
import {
  Button as ButtonMaterial,
  Divider,
  Container,
  IconButton,
  Grid,
  Typography,
  Box,
} from "@material-ui/core";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";

// Fields
import { StringField } from "./stringField";
import { DateField } from "./dateField";
import { BooleanField } from "./booleanField";
import { SelectField } from "./selectField";

import { ValidationSchema } from "./validationSchema";

import { currentTableActions } from "../../store/actions/currentTable.action";
import { transactionalActions } from "../../constants";

export const DynamicForm = (props) => {
  const dispatch = useDispatch();
  const tableName = props.match.params.tableName;
  const tableTitle = props.match.params.tableTitle;
  const serviceConfiguration = { tableName: tableName };
  const btnLabel =
    props.match.params.action === transactionalActions.EDIT
      ? "Update data"
      : "Save data";

  //selectors
  const currentTableReducer = useSelector((state) => state.currentTableReducer);
  const singleRegisterReducer = useSelector(
    (state) => state.singleRegisterReducer
  );
  const notificationReducer = useSelector((state) => state.notificationReducer);
  const descriptiveDataReducer = useSelector(
    (state) => state.descriptiveDataReducer
  );

  //states
  const [stateSwitch, setStateSwitch] = useState(true);
  const [PKName, setPKName] = useState(null);
  const [PKValue, setPKValue] = useState(null);

  //Controls for the Form
  const [formsValues, setformsValues] = useState({});
  const [formHelpers, setFormHelpers] = useState({});
  const [touched, setTouched] = React.useState({});

  const formik = useFormik({
    enableReinitialize: true,
    initialValues: {},
    onSubmit: (values, { resetForm }) => {
      values = formsValues;
      var tempValidations = {};
      for (let oneField of Object.entries(values)) {
        let errorMsg = validateOnly(oneField[0], oneField[1]);
        tempValidations = { ...tempValidations, [oneField[0]]: errorMsg };
      }
      setFormHelpers(tempValidations);
      var amountOfErrors = 0;
      Object.entries(tempValidations).map((oneHelper) => {
        if (oneHelper[1] !== null) {
          amountOfErrors++;
        }
      });
      console.log("action = " + props.match.params.action);
      if (amountOfErrors === 0) {
        props.match.params.action === transactionalActions.ADD
          ? dispatch(
              currentTableActions.addNewData(values, serviceConfiguration)
            )
          : dispatch(
              currentTableActions.updateData(
                values,
                PKName,
                PKValue,
                serviceConfiguration
              )
            );
      }
    },
  });

  //const -> Functions
  const handleReturnBtn = () => {
    dispatch(currentTableActions.clearSingleRegisterData());
    props.history.push(`/${tableName}/${tableTitle}`);
  };

  const handleChangeSwitch = (event) => {
    setStateSwitch(event.target.checked);
    setTouched({ ...touched, [event.target.name]: true });
    setformsValues({
      ...formsValues,
      [event.target.name]: event.target.checked,
    });
  };

  const handleFormsValue = (event) => {
    setTouched({ ...touched, [event.target.name]: true });
    validateAndSave(event.target.name, event.target.value);
  };

  const handleDates = ({ event, nameField }) => {
    validateAndSave(nameField, event);
    setTouched({ ...touched, [nameField]: true });
  };

  const handleDescriptive = ({ event, nameField }) => {
    validateAndSave(nameField, event.value);
    setTouched({ ...touched, [nameField]: true });
  };

  const validateAndSave = (nameField, value) => {
    const field = currentTableReducer.result.model.find(
      (item) => item.columnName === nameField
    );
    var errorMsg;
    [errorMsg, value] = ValidationSchema(field, value, formsValues);
    setFormHelpers({ ...formHelpers, [nameField]: errorMsg });
    /** Save values in the formsValue state */
    setformsValues({
      ...formsValues,
      [nameField]: value,
    });
  };

  const validateOnly = (nameField, value) => {
    const field = currentTableReducer.result.model.find(
      (item) => item.columnName === nameField
    );
    var errorMsg = null;
    if (field !== undefined) {
      [errorMsg, value] = ValidationSchema(field, value, formsValues);
    }
    return errorMsg;
  };

  const getDefaultValues = () => {
    let defValues = {};
    let today = new Date();
    if (currentTableReducer && currentTableReducer.result) {
      currentTableReducer.result.model.map((field) => {
        switch (field.columnType) {
          case "date":
            defValues = { ...defValues, [field.columnName]: today };
            break;
          case "boolean":
            defValues = { ...defValues, [field.columnName]: true };
            break;
          case "string":
            defValues = { ...defValues, [field.columnName]: "" };
            break;
          default:
            defValues = { ...defValues, [field.columnName]: "" };
            break;
        }
      });
    }
    return defValues;
  };

  useEffect(() => {
    /** Setting to avoid empty values when the field is not changed and aparently is fulled */
    let defValues = getDefaultValues();
    setformsValues(defValues);
    /** Getting descriptive data */
    currentTableReducer.result.model.map((field) => {
      if (field.props.isDescriptedByForeign) {
        dispatch(
          currentTableActions.getDescriptiveData(
            field.props.descriptionModel,
            field.props.descriptionField,
            field.columnName
          )
        );
      }
    });
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []); //missing dependencies because it needs to be triggered when the aplication starts only.

  //Edit Only
  useEffect(() => {
    if (props.match.params.action === transactionalActions.EDIT) {
      dispatch(
        currentTableActions.getSingleRegisterData(
          "id",
          props.match.params.id,
          serviceConfiguration
        )
      );
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [dispatch, props.match.params]); //missing dependence serviceConfiguration because it would not be triggered in the right mooment

  useEffect(() => {
    if (notificationReducer.success) {
      let defValues = getDefaultValues();
      setformsValues(defValues);
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [notificationReducer]); //missing dependence getDefaultValues because it would not be triggered in the right mooment

  useEffect(() => {
    if (singleRegisterReducer.result && currentTableReducer.result) {
      setformsValues(singleRegisterReducer.result.oneRegisterData);
      setPKName(currentTableReducer.result.tablePK);
      setPKValue(
        singleRegisterReducer.result.oneRegisterData[
          currentTableReducer.result.tablePK
        ]
      );
    }
  }, [singleRegisterReducer, currentTableReducer.result]);

  return (
    <>
      <Container>
        <Grid container direction="row" justify="center" alignItems="center">
          <Grid item xs={1}>
            <IconButton onClick={handleReturnBtn}>
              <ArrowBackIcon />
            </IconButton>
          </Grid>
          <Grid item xs={11}>
            <Grid
              container
              direction="row"
              justify="center"
              alignItems="center"
            >
              <Grid item xs={4}>
                <Typography component="div">
                  <Box fontSize={24} fontFamily="fontFamily">
                    {
                      /** Title page incoming */
                      props.match.params.action +
                        " " +
                        props.match.params.tableTitle
                    }
                  </Box>
                </Typography>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </Container>
      <Container style={{ marginTop: "1rem" }}>
        <form onSubmit={formik.handleSubmit}>
          <Grid container spacing={3}>
            {currentTableReducer && currentTableReducer.result
              ? currentTableReducer.result.model.map((field) => {
                  if (field.props.inputInForm) {
                    switch (field.columnType) {
                      case "date":
                        return (
                          <DateField
                            key={field.columnName}
                            titleField={field.columnDescriptiviveName}
                            nameField={field.columnName}
                            formsValues={formsValues}
                            handleDates={handleDates}
                            formHelpers={formHelpers}
                          />
                        );
                      case "string":
                        return (
                          <StringField
                            key={field.columnName}
                            titleField={field.columnDescriptiviveName}
                            nameField={field.columnName}
                            handleFormsValue={handleFormsValue}
                            formsValues={formsValues}
                            formHelpers={formHelpers}
                          />
                        );
                      case "email":
                        return (
                          <StringField
                            key={field.columnName}
                            titleField={field.columnDescriptiviveName}
                            nameField={field.columnName}
                            handleFormsValue={handleFormsValue}
                            formsValues={formsValues}
                            formHelpers={formHelpers}
                          />
                        );
                      case "boolean":
                        return (
                          <BooleanField
                            key={field.columnName}
                            titleField={field.columnDescriptiviveName}
                            nameField={field.columnName}
                            stateSwitch={stateSwitch}
                            handleChangeSwitch={handleChangeSwitch}
                            formsValues={formsValues}
                            formHelpers={formHelpers}
                          />
                        );
                      case "number":
                        if (
                          field.props.isDescriptedByForeign &&
                          descriptiveDataReducer &&
                          descriptiveDataReducer.result
                        ) {
                          return (
                            <SelectField
                              key={field.columnName}
                              nameField={field.columnName}
                              handleDescriptive={handleDescriptive}
                              formHelpers={formHelpers}
                              optionsData={
                                descriptiveDataReducer.result[field.columnName]
                              }
                              value={formsValues[field.columnName]}
                            />
                          );
                        } else {
                          return (
                            <StringField
                              key={field.columnName}
                              titleField={field.columnDescriptiviveName}
                              nameField={field.columnName}
                              handleFormsValue={handleFormsValue}
                              formsValues={formsValues}
                              formHelpers={formHelpers}
                            />
                          );
                        }
                      default:
                        return null;
                    }
                  }
                  return null;
                })
              : null}
            <Divider
              style={{ marginTop: "2rem", marginBottom: "2rem", width: "100%" }}
            />
            <div className="divSubmit">
              <ButtonMaterial
                className="btnSubmit"
                variant="contained"
                type="submit"
              >
                {btnLabel}
              </ButtonMaterial>
            </div>
          </Grid>
        </form>
      </Container>
      {/* <h5>Touched: {JSON.stringify(touched)}</h5>
      <h5>Helpers: {JSON.stringify(formHelpers)}</h5> */}
    </>
  );
};
