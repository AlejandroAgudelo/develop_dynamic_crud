import React from "react";
import Select from "react-select";
import { FormHelperText, Grid, FormControl } from "@material-ui/core";


const SelectField = ({
  nameField,
  handleDescriptive,
  formHelpers,
  optionsData,
  value
}) => {
  let options = [];
  let defaultObj = {}
  if (optionsData !== undefined) {
    optionsData.data.forEach(function (valor, index) {
      options.push({ value: valor.id, label: valor.name });
      if (valor.id === value) {
        defaultObj = { value: valor.id, label: valor.name }
      }
    });
  }

  return (
    <Grid item xs={6}>
      <div className="inputForm">
        <FormControl style={{ width: "100%" }}>
          <Select options={options} onChange={(event)=>handleDescriptive({ event, nameField })} value={value ? defaultObj : null} />
          <FormHelperText
            id={"helper" + nameField}
            style={{ color: "#F01C0F" }}
          >
            {formHelpers[nameField] ? formHelpers[nameField] : null}
          </FormHelperText>
        </FormControl>
      </div>
    </Grid>
  );
};

export { SelectField };
