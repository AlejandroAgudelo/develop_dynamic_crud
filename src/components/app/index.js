import React, { useEffect } from "react";

import Routes from "../../routes";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

//Notifications
import { showToast } from "../tools/toast";
import { clearNotification } from "../../store/actions/notification.action";

//Redux Store
import { useSelector, useDispatch } from "react-redux";

function App() {
  const notifications = useSelector((state) => state.notificationReducer);
  const dispatch = useDispatch();

  useEffect(() => {
    if (notifications && notifications.error) {
      console.log(notifications)
      const msg = notifications.msg ? notifications.msg : "Error";
      showToast("ERROR", msg);
      dispatch(clearNotification());
    }
    if (notifications && notifications.success) {
      const msg = notifications.msg ? notifications.msg : "Success";
      showToast("SUCCESS", msg);
      dispatch(clearNotification());
    }
  }, [notifications, dispatch]);
  return (
    <>
      <Routes />
      <ToastContainer />
    </>
  );
}

export { App };
