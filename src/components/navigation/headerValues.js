import { modelConstantNames } from '../../constants';

export const HeaderValues = [
  { tableName: modelConstantNames.APPROVAL_ATTORNEY, tableTitle: "Approval Attorneys" },
  { tableName: modelConstantNames.CITY, tableTitle: "City table" },
  { tableName: modelConstantNames.CREDIT_ENTITY, tableTitle: "Credit entity" },
];
