import React from "react";
import { Link, withRouter } from "react-router-dom";

import "./styles.css";

import { HeaderValues } from "./headerValues";

const HeaderMenu = (props) => {

  return (
    <>
      <nav className={`navbar fixed-top dash_layout`}>
          <Link
            style={{ fontFamily: "Fredoka One" }}
            to="/"
            className="navbar-brand d-flex align-items-center"
          >
            Home
          </Link>
          <h3 className="separator">|</h3>
          {/* <Link
          style={{ fontFamily: "Fredoka One" }}
          className="separator"
          to="/"
        >
          |
        </Link> */}
          {HeaderValues
            ? HeaderValues.map((singleValue) => (
                <Link
                  key={singleValue.tableName}
                  style={{ fontFamily: "Fredoka One" }}
                  to={`/${singleValue.tableName}/${singleValue.tableTitle}`}
                  className="headerValueName"
                >
                  {singleValue.tableTitle}
                </Link>
              ))
            : null}
      </nav>
    </>
  );
};

export default withRouter(HeaderMenu);
