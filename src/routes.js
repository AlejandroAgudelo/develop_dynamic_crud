import React from "react";
import { Switch, Route, BrowserRouter } from "react-router-dom";

/**Components*/
import { DynamicForm } from "./components/dynamicForm";
import { DynamicTable } from "./components/dynamicTable";
import { Home } from './components/modules/home'

import HeaderMenu from "./components/navigation/header";

const Routes = () => {
  return (
    <BrowserRouter>
      <HeaderMenu />
      <Switch>
        <Route path="/:tableName/:tableTitle/:action/:id" component={DynamicForm} />
        <Route path="/:tableName/:tableTitle" component={DynamicTable} />
        <Route path="/" component={Home} />
      </Switch>
    </BrowserRouter>
  );
};

export default Routes;
