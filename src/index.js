import React from "react";
import ReactDOM from "react-dom";

import { Provider } from "react-redux";
import ReduxStore from "./store";

import { App } from "./components/app";

//date picker
import { MuiPickersUtilsProvider } from "@material-ui/pickers";
import esLocale from "date-fns/locale/es";
import DateFnsUtils from "@date-io/date-fns";

ReactDOM.render(
  <MuiPickersUtilsProvider utils={DateFnsUtils} locale={esLocale}>
    {/* <React.StrictMode> */}
    <Provider store={ReduxStore()}>
      <App />
    </Provider>
    {/* </React.StrictMode> */}
  </MuiPickersUtilsProvider>,
  document.getElementById("root")
);
